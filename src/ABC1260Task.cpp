// ============================================================================
//
// = CONTEXT
//    TANGO Project - Task to gather Neutron monitor data
//
// = File
//    ABC1260Task.cpp
//
// = AUTHOR
//    X. Elattaoui - SOLEIL
//
// ============================================================================

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include <tango.h>
#include <math.h>
#include <yat/threading/Mutex.h>
#include <yat/time/Time.h>
#include <yat/utils/String.h> //- to lower/upper
#include <Xstring.h>
#include "ABC1260Task.hpp"

const size_t PERIODIC_MSG_PERIOD = 4000; //- in ms
// ============================================================================
// SOME USER DEFINED MESSAGES
// ============================================================================
const size_t kRESET_CMD                 = (yat::FIRST_USER_MSG + 10);
const size_t kBATTERY_CHARGE_CMD        = (yat::FIRST_USER_MSG + 20);
const size_t kGET_DOSE_ALARM_SETTINGS   = (yat::FIRST_USER_MSG + 30);
const size_t kGET_RATE_ALARM_SETTINGS   = (yat::FIRST_USER_MSG + 31);
const size_t kGET_4HRATE_ALARM_SETTINGS = (yat::FIRST_USER_MSG + 32);

const size_t kUNLOCK_CMDs_MSG = (yat::FIRST_USER_MSG + 1000);
const size_t kLOCK_CMDS_AFTER = 15000;           //- lock cmds after 15s

const double k4_HOURS_RESET_VALUE = (3600. * 4.); //- convert 4h in seconds

namespace ABC1260_ns
{
// ======================================================================
// ABC1260Task::ABC1260Task
// ======================================================================
ABC1260Task::ABC1260Task (Tango::DeviceImpl* host_device,
                          std::string dsProxyName, 
                          std::string protocole,
                          std::vector< std::string >& disableAlarmList,
                          double gammaGain, 
                          bool isABC1260)
  : yat4tango::DeviceTask(host_device),
    m_reset_cmd_unlocked(false),
    m_pwdInClear(""),
    m_pwdDecrypted(""),
    m_kn(0),
    m_kd(0),
    m_host_device(host_device),
    m_proxyDeviceName(dsProxyName),
    m_com_p(0),
    m_protocole(protocole),
    m_batteryCharge(""),
    m_isABC1260(isABC1260),
    m_gammaGain(gammaGain),
    m_vialType(""),
    m_vialName(""),
    m_vialCalFact(0),
    m_vialInitLoad(0),
    m_serialNum(""),
    m_firmwareVersion(""),
    m_responseStatusStr(""),
    m_battery_disabled(false),
    m_vial_disabled(false),
    m_dose_disabled(false),
    m_rate_disabled(false)
{
  m_codesList.clear();
  //- allocate memory for alarms settings (will check once at init !)
  //- dose
  m_dose_alarm_settings  = new Tango::DevVarDoubleStringArray();
  m_dose_alarm_settings->dvalue.length(1);
  m_dose_alarm_settings->dvalue[0] = 0.0;
  m_dose_alarm_settings->svalue.length(1);
  //- rate
  m_rate_alarm_settings  = new Tango::DevVarDoubleStringArray();
  m_rate_alarm_settings->dvalue.length(1);
  m_rate_alarm_settings->dvalue[0] = 0.0;
  m_rate_alarm_settings->svalue.length(1);
  //- 4hour rate
  m_4h_rate_alarm_settings  = new Tango::DevVarDoubleStringArray();
  m_4h_rate_alarm_settings->dvalue.length(1);
  m_4h_rate_alarm_settings->dvalue[0] = 0.0;
  m_4h_rate_alarm_settings->svalue.length(1);

  m_abcData.reset();

  //- disable alarm(s) : do not change device state to ALARM if disabled
  std::size_t nb_alarms = disableAlarmList.size();
  for( std::size_t i=0; i<nb_alarms; i++ )
  {
    std::string alarm_name = disableAlarmList[i];
    yat::StringUtil::to_lower(&alarm_name);

    //- skip line which begin with a "#" character
    if( alarm_name.find("#") != std::string::npos )
    { 
      continue;
    }
    if( alarm_name.find("batte") != std::string::npos )
    {
      m_battery_disabled = true;
    }
    if( alarm_name.find("vial") != std::string::npos )
    {
      m_vial_disabled = true;
    }
    if( alarm_name.find("dose") != std::string::npos )
    {
      m_dose_disabled = true;
    }
    if( alarm_name.find("rate") != std::string::npos )
    {
      m_rate_disabled = true;
    }
  }
}

// ======================================================================
// ABC1260Task::~ABC1260Task
// ======================================================================
ABC1260Task::~ABC1260Task (void)
{
  if ( m_dose_alarm_settings )
  {
    delete m_dose_alarm_settings;
    m_dose_alarm_settings = 0;
  }
  if ( m_rate_alarm_settings )
  {
    delete m_rate_alarm_settings;
    m_rate_alarm_settings = 0;
  }
  if ( m_4h_rate_alarm_settings )
  {
    delete m_4h_rate_alarm_settings;
    m_4h_rate_alarm_settings = 0;
  }
}

// ============================================================================
// ABC1260Task::process_message
// ============================================================================
void ABC1260Task::process_message (yat::Message& _msg) throw (Tango::DevFailed)
{
  DEBUG_STREAM << "ABC1260Task::handle_message::receiving msg " << _msg.to_string() << std::endl;

  //- handle msg
  switch (_msg.type())
  {
  //- THREAD_INIT ----------------------
  case yat::TASK_INIT:
    {
      DEBUG_STREAM << "ABC1260Task::handle_message::THREAD_INIT::thread is starting up" << std::endl;

      //- "initialization" code goes here
      //----------------------------------------------------
      yat::Timer t;

      //- create communication proxy
      init_communication();

      //- initialize reset timer : found time (in seconds) to the next reset
      initialize_4h_reset_timer();

      update_dose_alarm_settings();
      //- data updated only once
      update_serial_number();
      update_firmware_version();
      update_vial_data();

      update_rate_alarm_settings();
      update_4hour_rate_alarm_settings();

      //- update ABC STATE
      // update_ABC1260_state();
      //- update ABC STATUS
      update_ABC1260_status();

      //- configure optional msg handling
      set_periodic_msg_period(PERIODIC_MSG_PERIOD);
      enable_timeout_msg(false);
      enable_periodic_msg(true);
      DEBUG_STREAM << "\tABC1260Task::TASK_INIT finished in " << t.elapsed_msec() << " ms." << std::endl;
    }
    break;
  //- THREAD_EXIT ----------------------
  case yat::TASK_EXIT:
    {
      DEBUG_STREAM << "\tABC1260Task::handle_message::THREAD_EXIT::thread is quitting" << std::endl;

      //- "release" code goes here
      //----------------------------------------------------
      try
      {
        close_communication();
      }
      catch(...)
      {
        //- Noop
      }
    }
    break;
  //- THREAD_PERIODIC ------------------
  case yat::TASK_PERIODIC:
    {
      DEBUG_STREAM << "\n\n\tABC1260Task::handle_message::handling THREAD_PERIODIC msg" << std::endl;

      //- code relative to the task's periodic job goes here
      //----------------------------------------------------
      yat::Timer t;

      //- check if reset cmd has been unlocked
      if ( m_reset_cmd_unlocked )
      {
        //- if time elapsed, lock the reset cmd
        if ( m_unlockCMD_timer.elapsed_msec() > kLOCK_CMDS_AFTER )
          m_reset_cmd_unlocked = false;
      }

      //- check if it is time to send reset (4h timer as expired ?)
      if ( m_automatic_reset_timer.expired() )
      {
        //- send reset cmd
        reset_i();
        //- re-initialize the 4h counter for another period (4h)
        initialize_4h_reset_timer();
      }
      else
      {
        //- time management : Timestamp values

        // TODO  : cf YAT time
        struct tm* newtime;
        time_t last_reset_time;
        time( &last_reset_time );                /* Get time as long integer. */
        newtime = localtime( &last_reset_time ); /* Convert to local time.  */
        {
          //- copy in critical section
          yat::AutoMutex<> guard(m_data_mutex);
          //- update time since reset
          m_abcData.timeSinceReset = m_automatic_reset_timer.time_to_expiration();
          //- updtate timestamp value
          m_abcData.timestamp = asctime( newtime );
        }
        //- update ABC DOSE data
        update_dose_data();
        //- update ABC RATE data
        update_rate_data();
        //- update ABC remaining drops
        update_remainingDrops_data();
        //- update ABC STATUS
        update_ABC1260_status();
      }
      DEBUG_STREAM << "\t******ABC1260Task::TASK_PERIODIC done (with STATUS) in : " << t.elapsed_msec() << " ms.\n" << std::endl;
    }
    break;
  //- THREAD_TIMEOUT -------------------
  case yat::TASK_TIMEOUT:
    {
      //- code relative to the task's tmo handling goes here

    }
    break;
  //- BATTERY_CHARGE -------------------
  case kBATTERY_CHARGE_CMD:
    {
      //- code relative to the task's tmo handling goes here
      static std::string bChgCmd  = "c";

      try
      {
        m_batteryCharge = write_read(bChgCmd);
      }
      catch(...)
      {
        m_batteryCharge = "unknown";
      }
    }
    break;
  //- UNLOCK CMD MSG -----------------
  case kUNLOCK_CMDs_MSG:
    {
      //- decode crypted list of codes
      decrypt_codes();

      //- compare passwords and unlock cmds if OK
      unlock_cmds_i();

      //- restart timer
      if ( m_reset_cmd_unlocked )
      {
        m_unlockCMD_timer.restart();
      }
    }
    break;
  //- RESET MSG -----------------
  case kRESET_CMD:
    {
      //- check the command has been unlocked
      if ( m_reset_cmd_unlocked )
      {
        //- reset the neutron monitor
        reset_i();
      }
    }
    break;
  //- UNHANDLED MSG --------------------
  default:
    FATAL_STREAM << "ABC1260Task::handle_message::unhandled msg type received" << std::endl;
    break;
  }
}

// ============================================================================
// ABC1260Task::get_abc1260_data
// ============================================================================
void ABC1260Task::get_abc1260_data (ABC1260Data& dest)
{
  yat::AutoMutex<> guard(m_data_mutex);

  dest = m_abcData;
}

// **************************************************************************
//            get_battery_charge
//
//    returns the neutron monitor battery charge
//    send 'c' command to the ABC
//    argin : no argin
//    argout : no argout
//
// **************************************************************************
std::string ABC1260Task::get_battery_charge()
{
  yat::Timer t;
  //- create and post msg
  yat::Message* msg = new yat::Message(kBATTERY_CHARGE_CMD, MAX_USER_PRIORITY,true);
  if ( !msg )
  {
    ERROR_STREAM << "ABC1260Task::get_battery_charge-> yat::Message allocation failed." << std::endl;
    Tango::Except::throw_exception ("OUT_OF_MEMORY",
                                    "yat::Message allocation failed",
                                    "ABC1260Task::get_battery_charge");
  }

  //- wait till the message is processed !!
  wait_msg_handled(msg, PERIODIC_MSG_PERIOD);

  return m_batteryCharge;
}

// **************************************************************************
//            update_dose_data
//
//    send 'd' command to the ABC and update all 'dxxx' attributes then
//    argin : no argin
//    argout : no argout
//
// **************************************************************************
void ABC1260Task::update_dose_data( )
{
  yat::Timer t;
  static std::string doseCmd("D");
  std::string response("");
  std::string delims(",");
  std::string::size_type begIdx, endIdx;

  //- send dose command
  try
  {
    response = write_read(doseCmd);
    DEBUG_STREAM << "\t\tABC1260Task::update_dose_data :response:" << response << std::endl;

    if ( !response.empty() )
    {
      //- search beginning of the first data
      begIdx = response.find_first_not_of(delims);
      endIdx = response.find_first_of(delims, begIdx);
      std::string totalDose_str = response.substr(begIdx,(endIdx-begIdx));

      begIdx = endIdx + 1;
      endIdx = response.find_first_of(delims, begIdx);
      std::string doseSinceReset_str = response.substr(begIdx,(endIdx-begIdx));
//- TODO : check this!      _doseSinceReset *= _gammaGain;

      begIdx = endIdx + 1;
      endIdx = response.find_first_of(delims, begIdx);
      std::string totalCounts_str = response.substr(begIdx,(endIdx-begIdx));

      begIdx = endIdx + 1;
      endIdx = response.find_first_of(delims, begIdx);
      std::string countsSinceReset_str = response.substr(begIdx,(endIdx-begIdx));

      //- prepare data
      ABC1260Data data;
      data.totalDose   = XString<double>::convertFromString(totalDose_str);
      data.totalDose  *= m_gammaGain;
      data.doseSinceReset = XString<double>::convertFromString(doseSinceReset_str);
      data.totalCounts = XString<long>::convertFromString(totalCounts_str);
      data.countsSinceReset = XString<long>::convertFromString(countsSinceReset_str);
      begIdx = endIdx + 1;
      data.temperature = XString<double>::convertFromString(response.substr(begIdx));

      //- copy data in critical section
      {
        yat::AutoMutex<> guard(m_data_mutex);
        m_abcData.totalDose = data.totalDose;
        m_abcData.doseSinceReset = data.doseSinceReset;
        m_abcData.totalCounts = data.totalCounts;
        m_abcData.countsSinceReset = data.countsSinceReset;
        m_abcData.temperature = data.temperature;
      }
    }
    else
    {
      FATAL_STREAM << "ABC1260Task::update_dose_data -> NO RESPONSE RECEIVED !!!" << std::endl;
    }
    //- TODO : attr_integratedDose_read
    // construire une fenetre glissante sur 4 heures
  }
  catch(Tango::DevFailed& df)
  {
    ERROR_STREAM << "ABC1260Task::update_dose_data() : write read operation failed->DevFailed" << endl;
    Tango::Except::re_throw_exception(df,
                                      "INTERNAL_ERROR",
                                      "Write read operation failed.",
                                      "ABC1260Task::update_dose_data()");
  }
  catch(std::out_of_range& oor)
  {
    ERROR_STREAM << "\nABC1260Task::update_dose_data() : write read operation failed->[caught std::out_of_range]." << std::endl;
    ERROR_STREAM << "Description :\n" << oor.what() << std::endl;
    Tango::Except::throw_exception(
      "INTERNAL_ERROR",
      "Write read operation : caugth a std::out_of_range exception.Probably response from hardware is incoherent",
      "ABC1260Task::update_dose_data()");
  }
  catch(...)
  {
    ERROR_STREAM << "\nABC1260Task::update_dose_data() : write read operation failed->[caught...]" << endl;
    Tango::Except::throw_exception(
      "INTERNAL_ERROR",
      "Write read operation : caugth a [...] exception.",
      "ABC1260Task::update_dose_data()");
  }
}

// **************************************************************************
//            update_rate_data
//
//    send 'r' command to the ABC and update all 'rxxx' attributes
//    argin : no argin
//    argout : no argout
//
// **************************************************************************
void ABC1260Task::update_rate_data( )
{
  yat::Timer t;
  static std::string rateCmd = "r";
  std::string response = "";
  std::string delims(" ");
  std::string ident_apfel("("); //- old ABC1260 monitor
  std::string::size_type begIdx, endIdx;

  //- send rate command
  try
  {
    response = write_read(rateCmd);
    
    if ( !response.empty() )
    {
      //  first check if the firmware is not an old apfel, in that case the answer will contain an "("
      if(response.find(ident_apfel) != string::npos)
      {
        //- the command r returns 3 separated lines (but only 1 with ETHERNET!), read the second one
        if( m_protocole.find("SERIAL") != std::string::npos )
        {
          response = m_com_p->read();
          //- read last data
          response = m_com_p->read();
        }
      }
      else
      {
        //- extract data
        //- search beginning of the first data
        begIdx = response.find_first_not_of(delims);
        endIdx = response.find_first_of(delims, begIdx);
        // actually it is the dose, we should ingore the first number which has already been acquired with the dose
        //*attr_RCurrentRate_read = XString<double>::convertFromString(response.substr(begIdx,(endIdx-begIdx)));
        std::string curRateStr = response.substr(begIdx,(endIdx-begIdx));

        begIdx = endIdx + 1;
        begIdx = response.find_first_not_of(delims,begIdx);
        endIdx = response.find_first_of(delims, begIdx);
        // actually it is the dose, we should ingore the first number which has already been acquired with the dose
        //*attr_RCurrentCounts_read = XString<long>::convertFromString(response.substr(begIdx,(endIdx-begIdx)));
        std::string curCountStr = response.substr(begIdx,(endIdx-begIdx));

        begIdx = endIdx + 1;
        begIdx = response.find_first_not_of(delims,begIdx);
        endIdx = response.find_first_of(delims, begIdx);
        // _time4rate  = XString<long>::convertFromString(response.substr(begIdx,(endIdx-begIdx)));
        std::string time4RateStr = response.substr(begIdx,(endIdx-begIdx));

        begIdx = endIdx + 1;
        begIdx = response.find_first_not_of(delims,begIdx);
        endIdx = response.find_first_of(delims, begIdx);
        {
          yat::AutoMutex<> guard(m_data_mutex);
          m_abcData.doseRate = XString<double>::convertFromString(response.substr(begIdx,(endIdx-begIdx)));
          std::string lastCPURAteStr = response.substr(begIdx/*,(endIdx-begIdx)*/);
          m_abcData.doseRate *= m_gammaGain;
        }
      }
    }
    else
    {
      FATAL_STREAM << "ABC1260Task::update_rate_data -> NO RESPONSE RECEIVED !!!" << std::endl;
    }
  }
  catch(Tango::DevFailed& df)
  {

    ERROR_STREAM << "\nABC1260Task::update_rate_data() : write read operation failed->DevFailed" << endl;
    Tango::Except::re_throw_exception(df,
                                      "INTERNAL_ERROR",
                                      "Write read operation failed.",
                                      "ABC1260Task::update_rate_data()");
  }
  catch(std::out_of_range)
  {
    ERROR_STREAM << "\nABC1260Task::update_rate_data() : write read operation failed->[caught std::out_of_range]" << endl;
    Tango::Except::throw_exception(
      "INTERNAL_ERROR",
      "Write read operation : caugth a std::out_of_range exception.Probably response from hardware is incoherent",
      "ABC1260Task::update_rate_data()");
  }
  catch(...)
  {
    ERROR_STREAM << "\nABC1260Task::update_rate_data() : write read operation failed->[caugth...]" << endl;
    Tango::Except::throw_exception(
      "INTERNAL_ERROR",
      "Write read operation : caugth a [...] exception.",
      "ABC1260Task::update_rate_data()");
  }
}

// **************************************************************************
//            update_rate_data
//
//    send 's' command to the ABC
//    argin : no argin
//    argout : no argout
//
// **************************************************************************
void ABC1260Task::update_remainingDrops_data()
{
  static std::string rDropsCmd = "s";
  std::string response = "";
  //- send dose command
  try
  {
    response = write_read(rDropsCmd);

    if ( !response.empty() )
    {
      yat::AutoMutex<> guard(m_data_mutex);
      m_abcData.remainingDrops = XString<long>::convertFromString(response);
    }
    else
    {
      FATAL_STREAM << "ABC1260Task::update_remainingDrops_data -> NO RESPONSE RECEIVED !!!" << std::endl;
    }
  }
  catch(Tango::DevFailed& df)
  {
    ERROR_STREAM << "ABC1260Task::update_remainingDrops_data() : write read operation failed->DevFailed" << endl;
    Tango::Except::re_throw_exception(df,
                                      "INTERNAL_ERROR",
                                      "Write read operation failed.",
                                      "ABC1260Task::update_remainingDrops_data()");
  }
  catch(std::out_of_range& oor)
  {
    ERROR_STREAM << "\nABC1260Task::update_remainingDrops_data() : write read operation failed->[caught std::out_of_range]" << std::endl;
    ERROR_STREAM << "Details : \n" << oor.what() << std::endl;
    Tango::Except::throw_exception(
      "INTERNAL_ERROR",
      "Write read operation : caugth a std::out_of_range exception.Probably response from hardware is incoherent",
      "ABC1260Task::update_remainingDrops_data()");
  }
  catch(...)
  {
    ERROR_STREAM << "\nABC1260Task::update_remainingDrops_data() : write read operation failed->[caught...]" << endl;
    Tango::Except::throw_exception(
      "INTERNAL_ERROR",
      "Write read operation : caugth a [...] exception.",
      "ABC1260Task::update_remainingDrops_data()");
  }
}

// **************************************************************************
//            update_ABC1260_status
//
//    send '=' command to the ABC
//    argin : no argin
//    argout : no argout
//
// **************************************************************************
void ABC1260Task::update_ABC1260_status( )
{
  static std::string statusCmd  = "=";    //- Cmd to get alarm status
  std::string resp("");

  try
  {
    resp = write_read(statusCmd);
  }
  catch(...)
  {
    resp.clear();
    ERROR_STREAM << "ABC1260Task::update_ABC1260_status Failed : caught [...]" << std::endl;
  }
  //- enter critical section
  {
    yat::AutoMutex<> guard(m_status_mutex);
    m_responseStatusStr.clear();
    //- send command
    m_responseStatusStr = resp;
  }
}

// **************************************************************************
//            update_serial_number
//
//    get the vial serial number
//    argin : no argin
//    argout : no argout
//
// **************************************************************************
void ABC1260Task::update_serial_number()
{
  static std::string serialNumberCMD = "-";

  yat::AutoMutex<> guard(m_serialNum_mutex);

  if ( m_isABC1260 )
  {
    try
    {
      //- send command
      m_serialNum = write_read(serialNumberCMD);
    }
    catch(...)
    {
      m_serialNum = "unkown";
    }
  }
  else
  {
    m_serialNum = "OLD APFEL firmware";
  }
}

// **************************************************************************
//            update_firmware_version
//
//    get the vial firmware version
//    argin : no argin
//    argout : no argout
//
// **************************************************************************
void ABC1260Task::update_firmware_version()
{
  static std::string firmwareVersionCMD = "~";

  yat::AutoMutex<> guard(m_firmwareVers_mutex);

  if ( m_isABC1260 )
  {
    try
    {
      //- send command
      m_firmwareVersion = write_read(firmwareVersionCMD);
    }
    catch(...)
    {
      m_firmwareVersion = "unkown";
    }
    //- send command
  }
  else
  {
    m_firmwareVersion = "OLD APFEL firmware";
  }
}

// **************************************************************************
//            update_vial_data
//
//    get the vial parameters
//    argin : no argin
//    argout : no argout
//
// **************************************************************************
void ABC1260Task::update_vial_data( )
{
  yat::Timer t;
  static std::string VialCmd = "v";
  std::string response = "";
  std::string delims(" ");
  
  /* NOTE :
    - response : 1 SDD-100    1 SDD-0000-0000     130  45000
    - response length : 43
  */

  try
  {
    //- send  command
    response = write_read(VialCmd);

    if ( response.empty() )
    {
      //- TODO : state/status
      ERROR_STREAM << "\t ABC1260Task::update_vial_data() -> NO RESPONSE !!! " << endl;
      return;
    }
  }
  catch(...)
  {
    response.clear();
  }

  if(response.length() < 43 || response.empty() )
  {
    FATAL_STREAM << "\nABC1260Task::update_vial_data() 1 : write read operation failed->[caught std::out_of_range]" << endl;
    Tango::Except::throw_exception(
      "INTERNAL_ERROR",
      "Write read operation 1 : caugth a std::out_of_range exception.Probably response from hardware is incoherent",
      "ABC1260Task::update_vial_data()");
  }

  //- TODO : use tokeniser !
  yat::AutoMutex<> guard(m_vial_mutex);
  m_vialType = response.substr(2,10);

  m_vialName = response.substr(13,14);

  m_vialCalFact = XString<long>::convertFromString(response.substr(28,6));

  m_vialInitLoad = XString<long>::convertFromString(response.substr(35,6));
}

// **************************************************************************
//            update_dose_alarm_settings
//
//    get the vial parameters
//    argin : no argin
//    argout : no argout
//
// **************************************************************************
void ABC1260Task::update_dose_alarm_settings( )
{
  static std::string doseALsetCMD = "1";
  std::string response = "";
  double alDoseThres = 0.;
  short alDoseValid;

  //- get DOSE alarm settings
  response = get_alarm_settings(doseALsetCMD, alDoseThres,alDoseValid);

  if ( !response.empty() )
  {
    yat::AutoMutex<> guard(m_4hRateAlarmSettings_mutex);
    m_dose_alarm_settings->svalue[0] = CORBA::string_dup(response.c_str());
    m_dose_alarm_settings->dvalue[0] = alDoseThres;
  }
}

// **************************************************************************
//            update_rate_alarm_settings
//
//    get the vial parameters
//    argin : no argin
//    argout : no argout
//
// **************************************************************************
void ABC1260Task::update_rate_alarm_settings( )
{
  static std::string rateALsetCMD = "2";
  std::string response = "";
  double alRateThres = 0.;
  short alRateValid;

  //- get RATE alarm settings
  response = get_alarm_settings(rateALsetCMD, alRateThres,alRateValid);

  if ( !response.empty() )
  {
    yat::AutoMutex<> guard(m_4hRateAlarmSettings_mutex);
    m_rate_alarm_settings->svalue[0] = CORBA::string_dup(response.c_str());
    m_rate_alarm_settings->dvalue[0] = alRateThres;
  }
}

// **************************************************************************
//            update_4hour_rate_alarm_settings
//
//    get the vial parameters
//    argin : no argin
//    argout : no argout
//
// **************************************************************************
void ABC1260Task::update_4hour_rate_alarm_settings( )
{
  yat::Timer t;
  static std::string fourHrateALsetCMD = "3";
  std::string response = "";
  double al4hThres = 0.;
  short al4hValid;

  //- get RATE alarm settings
  response = get_alarm_settings(fourHrateALsetCMD, al4hThres,al4hValid);

  if ( !response.empty() )
  {
    yat::AutoMutex<> guard(m_4hRateAlarmSettings_mutex);
    m_4h_rate_alarm_settings->svalue[0] = CORBA::string_dup(response.c_str());
    m_4h_rate_alarm_settings->dvalue[0] = al4hThres;
  }
}

// **************************************************************************
//            get_alarm_settings
//
//    internal method to parse alarms settings
//
// **************************************************************************
std::string ABC1260Task::get_alarm_settings(std::string cmd_to_send, double& value, short& alValid )
{
  std::string response ("");
  std::string argout ("");

  try
  {
    //- Send command
    response = write_read(cmd_to_send);
    if ( response.empty() )
    {
      argout = "get_alarm_settings FAILED";
    }
    else
    {
      DEBUG_STREAM << "get_alarm_settings : response =*" << response << "*END" << std::endl;

      //- Extract data :
      //--------------
      //- 4 last digits are the dose alarm value :
      value = XString<double>::convertFromString( response.substr(1,response.find("\n")) );

      //- First value is the alarm state :
      alValid = XString<short>::convertFromString( response.substr(0,1) );

      //- Switch alarmStateValue return the enabled alarm
      if( !alValid )
      {
        argout = "Alarm OFF";
      }
      else if( alValid == 1 )
      {
        argout = "Alarm ON";
      }
      else if( alValid == 2 )
      {
        argout = "Alarm ON + External";
      }
    }
  }
  catch(...)
  {
    argout.clear();
    
    ERROR_STREAM << "ABC1260Task::get_alarm_settings Failed : caught [...]." << std::endl;
  }

  return argout;
}

// ============================================================================
// ABC1260Task::get_ABC1260_status
// ============================================================================
std::string ABC1260Task::get_ABC1260_status(Tango::DevState& state)
{
  yat::Timer t;
  std::stringstream s;
  std::string statusStr         ("");
  std::string rateAlarmStatus   ("");
  std::string iDoseAlarmStatus  ("");
  std::string doseAlarmStatus   ("");
  std::string vialAlarmStatus   ("");
  std::string batteryAlarmStatus("");
  std::string localStatus       ("");

  //- enter critical section
  {
    yat::AutoMutex<> guard(m_status_mutex);
    localStatus = m_responseStatusStr;
  }//- end critical section

    std::size_t lgth = localStatus.length();

  //- the response must be in the form : "0000 1100 0000 1100 1000" and length must be equal to 24 (characters)
  if ( localStatus.empty() || lgth < 24)
  {
    ERROR_STREAM << "ABC1260Task::get_ABC1260_status -> resp = " << localStatus << std::endl;
    statusStr = "No response or incomplete response received from the equipment !!";
    state = Tango::ALARM;
    return statusStr;
  }

  try
  {
    //- parse the received status string
    std::string delim(" "); //- space char used to separate each alarm info
    std::string::size_type begIdx = 0;
    std::string::size_type endIdx = localStatus.find(delim);
    //- rate alarm status data
    rateAlarmStatus = localStatus.substr(begIdx, (endIdx - begIdx));

    begIdx = endIdx + 1;
    endIdx = localStatus.find(delim, begIdx);
    //- integrated dose alarm status data
    iDoseAlarmStatus = localStatus.substr(begIdx, (endIdx - begIdx));

    begIdx = endIdx + 1;
    endIdx = localStatus.find(delim, begIdx);
    //- dose alarm status data
    doseAlarmStatus = localStatus.substr(begIdx, (endIdx - begIdx));

    begIdx = endIdx + 1;
    endIdx = localStatus.find(delim, begIdx);
    //- vial alarm status data
    vialAlarmStatus = localStatus.substr(begIdx, (endIdx - begIdx));

    begIdx = endIdx + 1;
    //- battery alarm status data
    batteryAlarmStatus = localStatus.substr(begIdx);
  }
  catch(...)
  {
      statusStr = "Status command response parsing failed. \n Received: " + localStatus;
      state = Tango::ALARM;
      return statusStr;
  }

  //- check device type
  if( m_isABC1260 )
  {
    //- Serial num and vial data are updated once (no mutex needed here)
    s << "ABC1260 s/n " << get_serial_number() << std::endl;
    s << "Vial type: "  << m_vialType << std::endl;
    s << "Vial name: "  << m_vialName << std::endl;
    if( m_vialCalFact != -1)
    {
      s << "Vial calibration factor: "<< m_vialCalFact  << std::endl;
    }
  }
  else
  {
    s << "OLD Apfel monitor " << std::endl ;
  }

  //- add gamma info
  if(m_gammaGain == 1.0)
  {
    s << "Mode : neutron measurement (no gamma gain)" << std::endl;
  }
  else
  {
    s << "Mode : neutron + gamma measurement (gain = " << m_gammaGain << ")" << std::endl;
  }

  //- parse each alarm status strings
  //- now parse each alarm status strings
  state = Tango::ON;
  char ENABLED = '1';
  s << "Rate Alarm : ";
  //- Rate Alarm STATUS
  if ( rateAlarmStatus.at(0) == ENABLED )
  {
    s << "ENABLED, ";
    if ( rateAlarmStatus.at(1) == ENABLED )
    {
      s << "EXTERNAL, ";
    }
    if ( rateAlarmStatus.at(2) == ENABLED )
    {
      s << "TRIPPED, ";
      if( !m_rate_disabled )
      {
        state = Tango::ALARM;
      }
    }
    else
    {
      s << "NOT TRIPPED, ";
    }
    if ( rateAlarmStatus.at(3) == ENABLED )
    {
      s << "DISPLAYED." << std::endl;
    }
    else
    {
      s << "NOT DISPLAYED." << std::endl;
    }
  }
  else
  {
    s << "DISABLED." << std::endl;
  }

  //- Integrated Dose Alarm STATUS
  s << "Integrated Dose Alarm : ";
  if ( iDoseAlarmStatus.at(0) == ENABLED )
  {
    s << "ENABLED, ";
    if ( iDoseAlarmStatus.at(1) == ENABLED )
    {
      s << "EXTERNAL, ";
    }
    if ( iDoseAlarmStatus.at(2) == ENABLED )
    {
      s << "TRIPPED, ";
      state = Tango::ALARM;
    }
    else
    {
      s << "NOT TRIPPED, ";
    }
    if ( iDoseAlarmStatus.at(3) == ENABLED )
    {
      s << "DISPLAYED." << std::endl;
    }
    else
    {
      s << "NOT DISPLAYED." << std::endl;
    }
  }
  else
  {
    s << "DISABLED." << std::endl;
  }

  s << "Dose Alarm : ";
  //- Dose Alarm STATUS
  if ( doseAlarmStatus.at(0) == ENABLED )
  {
    s << "ENABLED, ";
    if ( doseAlarmStatus.at(1) == ENABLED )
    {
      s << "EXTERNAL, ";
    }
    if ( doseAlarmStatus.at(2) == ENABLED )
    {
      s << "TRIPPED, ";
      if( !m_dose_disabled )
      {
        state = Tango::ALARM;
      }
    }
    else
    {
      s << "NOT TRIPPED, ";
    }
    if ( doseAlarmStatus.at(3) == ENABLED )
    {
      s << "DISPLAYED." << std::endl;
    }
    else
    {
      s << "NOT DISPLAYED." << std::endl;
    }
  }
  else
  {
    s << "DISABLED." << std::endl;
  }

  s << "Vial Alarm : ";
  //- Vial Alarm STATUS
  if ( vialAlarmStatus.at(0) == ENABLED )
  {
    s << "ENABLED, ";
    if ( vialAlarmStatus.at(1) == ENABLED )
    {
      s << "EXTERNAL, ";
    }
    if ( vialAlarmStatus.at(2) == ENABLED )
    {
      s << "TRIPPED, ";
      if( !m_vial_disabled )
      {
        state = Tango::ALARM;
      }
    }
    else
    {
      s << "NOT TRIPPED, ";
    }
    if ( vialAlarmStatus.at(3) == ENABLED )
    {
      s << "DISPLAYED." << std::endl;
    }
    else
    {
      s << "NOT DISPLAYED." << std::endl;
    }
  }
  else
  {
    s << "DISABLED.";
  }

  s << "Battery Alarm : ";
  //- Battery Alarm STATUS
  if ( batteryAlarmStatus.at(0) == ENABLED )
  {
    s << "ENABLED, ";
    if ( batteryAlarmStatus.at(1) == ENABLED )
    {
      s << "EXTERNAL, ";
    }
    if ( batteryAlarmStatus.at(2) == ENABLED )
    {
      s << "TRIPPED, ";
      if( !m_battery_disabled )
      {
        state = Tango::ALARM;
      }
    }
    else
    {
      s << "NOT TRIPPED, ";
    }
    if ( batteryAlarmStatus.at(3) == ENABLED )
    {
      s << "DISPLAYED." << std::endl;
    }
    else
    {
      s << "NOT DISPLAYED." << std::endl;
    }
  }
  else
  {
    s << "DISABLED.";
  }

  statusStr = s.str();

  return statusStr;
}

// ============================================================================
// ABC1260Task::unlock_reset_cmd
// ============================================================================
void ABC1260Task::unlock_reset_cmd()
{
  bool waitable = true;

  //- instanciate a message of type kDOUBLE_SCALAR_MSG
  yat::Message* msg = new yat::Message(kUNLOCK_CMDs_MSG, DEFAULT_MSG_PRIORITY, waitable);
  if ( !msg )
  {
    Tango::Except::throw_exception ("OUT_OF_MEMORY",
                                    "yat::Message allocation failed",
                                    "ABC1260Task::unlock_reset_cmd");
  }

  //- in case you want to wait for the msg to be handled before returning (synchronous approach)
  //- just do the following...
  wait_msg_handled(msg, 2000);
}

// ============================================================================
// ABC1260Task::reset
// ============================================================================
void ABC1260Task::reset()
{
  //- check the command has been unlocked
  if ( !m_reset_cmd_unlocked )
  {
    Tango::Except::throw_exception ("COMMAND_NOT_ALLOWED",
                                    "Reset command is locked!",
                                    "ABC1260Task::reset");
  }

  //- instanciate a message
  yat::Message* msg = new yat::Message(kRESET_CMD, DEFAULT_MSG_PRIORITY);
  if ( !msg )
  {
    Tango::Except::throw_exception ("OUT_OF_MEMORY",
                                    "yat::Message allocation failed",
                                    "ABC1260Task::reset");
  }

  //- in case you want to wait for the msg to be handled before returning (synchronous approach)
  //- just do the following...
  //wait_msg_handled(msg, 2000);
  //- Here only the command unlock is blocked to ensure the reset command is really unlocked
  //- then, the reset command is sent to the thread to be executed after the periodic msg which
  //- can take 5 seconds to finish!
  post(msg);
}

//-----------------------------------------------------------------------
//
//                            INTERNAL METHODS
//
//-----------------------------------------------------------------------
// ============================================================================
// ABC1260Task::init_communication
// ============================================================================
void ABC1260Task::init_communication()
{
  if( !m_com_p )
  {
    try
    {
      if ( !m_proxyDeviceName.empty() )
      {
        //- Creates the Proxy
        m_com_p = new Communication(m_host_device, m_proxyDeviceName, m_protocole);
        m_com_p->create_proxy();
      }
    }
    catch(std::bad_alloc& bd)
    {
      FATAL_STREAM << "ABC1260Task::init_communication() : OUT_OF_MEMORY unable to create proxy on : "
                   << m_proxyDeviceName
                   << std::endl;
      FATAL_STREAM << "Description :\n" << bd.what() << std::endl;
      close_communication();
    }
    catch(Tango::DevFailed& df)
    {
      close_communication();
      Tango::Except::print_exception(df);
    }
    catch(...)
    {
      FATAL_STREAM << "ABC1260Task::init_communication() : unable to create proxy, caught [...]. " << endl;
      close_communication();
    }
  }
}

// ============================================================================
// ABC1260Task::close_communication
// ============================================================================
void ABC1260Task::close_communication()
{
  if ( m_com_p )
  {
    delete m_com_p;
    m_com_p = 0;
  }
}

// **************************************************************************
//            ABC1260Task::write_read
//
//    send specific command to the ABC and return its response
//    argin : string command
//    argout : string response
//
//NOTE : Using an RS232-to-RS485 adpter, it needs 125ms to switch from SEND mode
//-----   to RECEIVE mode.
// **************************************************************************
std::string ABC1260Task::write_read(const std::string& cmd_to_send)
{
  long nb_char_written = 0;
  std::string response("");
  long readMode; //- SL_RAW

  if ( !m_com_p )
  {
    m_abcData.invalid_data();
    return "0000";  //- hint to invalid data and have the right state!!
  }

  try
  {
    //- send command
    response = m_com_p->write_read(cmd_to_send);
  }
  catch(Tango::DevFailed& df)
  {
    FATAL_STREAM << "Cannot read back data from the communciation device :\n" << df << std::endl;
    //- return invalid data
    m_abcData.invalid_data();
    yat::AutoMutex<> guard(m_status_mutex);
    m_responseStatusStr = "0000";
    Tango::Except::re_throw_exception(df,
                                      "CONNECTION_ERROR",
                                      "Cannot read back data from the communciation device.",
                                      "ABC1260Task::write_read()->DevSerReadRaw");
  }
  catch(...)
  {
    FATAL_STREAM << "Failed to read reponse of " << cmd_to_send << " command." << std::endl;
    //- return invalid data
    m_abcData.invalid_data();
    yat::AutoMutex<> guard(m_status_mutex);
    m_responseStatusStr = "0000";
    Tango::Except::throw_exception(
      "INTERNAL_ERROR",
      "Cannot read back data from the communciation device.",
      "ABC1260Task::write_read()->DevSerReadRaw");
  }

  // check if the protocol is echoing the input command. It occurs when we map an rs485 on a rs422
  // in that case, we skip the first character
  if(response[0] == cmd_to_send[0])
  {
    response = response.substr(1);
  }

  return response;
}

//-----------------------------------------------------------------------
//
//                           RESET MANAGEMENT
//
//-----------------------------------------------------------------------
void ABC1260Task::initialize_4h_reset_timer()
{
  //- set unit in seconds
  m_automatic_reset_timer.set_unit(yat::Timeout::TimeoutUnit::TMO_UNIT_SEC);
  //- get the time in seconds till the next reset
  yat::CurrentTime ct;
  double ct_secs = ct.hour() * 3600. + ct.minute() * 60. + ct.second();
  double fourHours_inSec = 3600. * 4.;

  double time_to_next_reset = fourHours_inSec - ( ::fmod(ct_secs, fourHours_inSec) );

  //- configure reset timer with the computed value
  m_automatic_reset_timer.set_value(time_to_next_reset);
  //- start the reset timer timeout to reset automatically the ABC1260
  m_automatic_reset_timer.restart();
}

// ============================================================================
// ABC1260Task::unlock_cmds_i
// ============================================================================
void ABC1260Task::unlock_cmds_i ()
{
  //- compare returns 0 if strings are equals
  if ( !m_pwdInClear.compare(m_pwdDecrypted) )
  {
    m_reset_cmd_unlocked = true;
  }
}

// ============================================================================
// ABC1260Task::reset_i
// ============================================================================
void ABC1260Task::reset_i ()
{
  std::string firstChar = "*";
  std::string secondChar= "Y";

  DEBUG_STREAM << "ABC1260::reset(): entering... !" << endl;

  //  Add your own code to control device here
  /****************************************/
  /* The procedure is as follows:         */
  /*  - Send '*' character                */
  /*  - then send a 'Y' character         */
  /****************************************/

  //- send the first character
  write_read(firstChar);

  //- send the second character
  write_read(secondChar);

  //- update last reset date value
  struct tm* newtime;
  time_t last_reset_time;
  //- update last reset date
  time( &last_reset_time );                /* Get time as long integer. */
  newtime = localtime( &last_reset_time ); /* Convert to local time.  */
  //- copy in critical section
  yat::AutoMutex<> guard(m_data_mutex);
  m_abcData.lastResetDate = asctime( newtime );

  INFO_STREAM << "\n\tABC1260Task::reset_i () -> SENT at : " << m_abcData.lastResetDate << std::endl;
}

// ============================================================================
// ABC1260Task::decrypt_codes
// ============================================================================
void ABC1260Task::decrypt_codes ()
{
  char l;
  std::string mdpResult("");

  for (unsigned long int idx = 0; idx < m_codesList.size(); ++idx)
  {
    l = decrypt(m_codesList.at(idx));
    mdpResult += l;
  }

  m_pwdDecrypted = mdpResult;
}

/****************************FONCTION DECRYPTAGE*******************************/
unsigned char ABC1260Task::decrypt(unsigned long int k)
{
  unsigned long int i,res = 1;
  unsigned char x;

  for(i = 0; i < m_kd; i++)
  {
    res = res * k;
    res = fmod((double)res,(double)m_kn);
  }

  x = ((unsigned char)res);

  return x;
}

} // namespace
