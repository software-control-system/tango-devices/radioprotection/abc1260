// ============================================================================
//
// = CONTEXT
//    TANGO Project - ABC1260 Data Class
//
// = FILENAME
//    ABC1260Data.hpp
//
// = AUTHOR
//    X. Elattaoui
// ============================================================================

#ifndef _ABC1260_DATA_H_
#define _ABC1260_DATA_H_

// ============================================================================
// DEPENDENCIES
// ============================================================================
//#include <tango.h>
#include <string>
#include <yat/Portability.h>


/**
 *  \brief ABC1260 Data
 *
 *  \author Xavier Elattaoui
 *  \date 07-2013
 */

 /**
 *  ABC1260Data class description:
 *    This class shares ABC1260 data between device and thread.
 */

class ABC1260Data
{
public:

  //- ctor
  ABC1260Data ()
  {
    this->reset();
  }

  //- copy ctor
  ABC1260Data (const ABC1260Data& src)
  {
    *this = src;
  }

  //- operator=
  const ABC1260Data& operator= (const ABC1260Data& src)
  {
    if (this != &src)
    {
      totalDose        = src.totalDose;
      totalCounts      = src.totalCounts;
      doseSinceReset   = src.doseSinceReset;
      countsSinceReset = src.countsSinceReset;
      doseRate	       = src.doseRate;
      timeSinceReset   = src.timeSinceReset;
      timestamp        = src.timestamp;
      externalAlarm    = src.externalAlarm;
      temperature      = src.temperature;
      lastResetDate    = src.lastResetDate;
      vialName	       = src.vialName;
      serialNumber     = src.serialNumber;
      remainingDrops   = src.remainingDrops;
    }
    return *this;
  }

  //- reset
  void reset ()
  {
    totalDose        = yat::IEEE_NAN;
    totalCounts      = yat::IEEE_NAN;
    doseSinceReset   = yat::IEEE_NAN;
    countsSinceReset = yat::IEEE_NAN;
    doseRate				 = yat::IEEE_NAN;
    timeSinceReset   = yat::IEEE_NAN;
    timestamp        = "NaN";
    externalAlarm    = false;
    temperature      = yat::IEEE_NAN;
    lastResetDate    = "NaN";
    vialName         = "NaN";
    serialNumber     = "NaN";
    remainingDrops   = yat::IEEE_NAN;
  }

  //- in case of error, invalid data : return NaN
  void invalid_data ()
  {
    reset();
  }

  double  		totalDose;
  long    		totalCounts;
  double  		doseSinceReset;
  long    		countsSinceReset;
  double  		doseRate;
  double  		timeSinceReset;
  std::string timestamp;
  bool    		externalAlarm;
  double  		temperature;
  std::string lastResetDate;
  std::string vialName;
  std::string serialNumber;
  long    		remainingDrops;
};

#endif // _ABC1260_DATA_H_
