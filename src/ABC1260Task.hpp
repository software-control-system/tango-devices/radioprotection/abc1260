// ============================================================================
//
// = CONTEXT
//		ABC1260Task
//
// = File
//		ABC1260Task.hpp
//
// = AUTHOR
//		X. Elattaoui Synchrotron Soleil France
//
// ============================================================================

#ifndef _ABC1260_TASK_H_
#define _ABC1260_TASK_H_

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include <tango.h>
#include <vector>
#include <yat4tango/DeviceTask.h>
#include <yat/time/Timer.h>        //- timer to lock Reset cmd
#include <DeviceProxyHelper.h>
#include "ABC1260Data.hpp"
#include "Communication.hpp"

/**
 *  ABC1260Task class description:
 *    This class reads dose and dose rate from the ABC1260 through a serial line.
 *    The communication interface used is RS485.
 *    No commands Start, Stop needed.
 */

namespace ABC1260_ns
{
// ============================================================================
// class: ABC1260Task
// ============================================================================
class ABC1260Task : public yat4tango::DeviceTask
{
public:
	//- ctor ---------------------------------
  ABC1260Task (Tango::DeviceImpl * host_device, 
                std::string dsProxyName,
                std::string protocol,
                std::vector< std::string >& disableAlarmList,
                double gammaGain = 1., 
                bool isABC1260=true);

	//- dtor ---------------------------------
	virtual ~ABC1260Task (void);

	//- getters ------------------------------
  std::string get_serial_number() {
    yat::AutoMutex<> guard(m_serialNum_mutex);
    return m_serialNum;
  }
  std::string get_firmware_version() {
    yat::AutoMutex<> guard(m_firmwareVers_mutex);
    return m_firmwareVersion;
  }
  std::string get_vial_type() {
    yat::AutoMutex<> guard(m_vial_mutex);
    return m_vialType;
  }
  std::string get_vial_name() {
    yat::AutoMutex<> guard(m_vial_mutex);
    return m_vialName;
  }
  long get_vial_calcFact() {
    yat::AutoMutex<> guard(m_vial_mutex);
    return m_vialCalFact;
  }
  long get_vial_initLoad() {
    yat::AutoMutex<> guard(m_vial_mutex);
    return m_vialInitLoad;
  }
  Tango::DevVarDoubleStringArray* get_dose_alarm_settings() {
		return m_dose_alarm_settings;
	}
	Tango::DevVarDoubleStringArray* get_rate_alarm_settings() {
		return m_rate_alarm_settings;
	}
  Tango::DevVarDoubleStringArray* get_4hour_rate_alarm_settings() {
		yat::AutoMutex<> guard(m_4hRateAlarmSettings_mutex);
		return m_4h_rate_alarm_settings;
	}
  std::string get_battery_charge();

	//- method to update all device attrs
  void get_abc1260_data (ABC1260Data & dest);

  //- State/status management
  std::string get_ABC1260_status(Tango::DevState&);

  //- RESET protection management
  inline void set_password(const std::string& pwd) {
    m_pwdInClear = pwd;
  }

  inline void set_n (unsigned long int n) {
    m_kn = n;
  }

  inline void set_d (unsigned long int d) {
    m_kd = d;
  }

  inline void set_codesList (std::vector<unsigned long int>& codesList) {
    m_codesList.assign(codesList.begin(), codesList.end());
  }

	//- reset management
  void unlock_reset_cmd();

  void reset();

  inline bool is_reset_cmd_unlocked () {
    return m_reset_cmd_unlocked;
  }

protected:

  //- process_message (implements yat4tango::DeviceTask pure virtual method)
	virtual void process_message (yat::Message& msg) throw (Tango::DevFailed);

  //- HW access
  std::string write_read(const std::string& cmd_to_send);

private:
  void init_communication();
  void close_communication();

  //- HW data : updated at init (read only once)
  void update_serial_number		();
  void update_firmware_version();
  void update_vial_data				();
  void update_dose_alarm_settings();
  void update_rate_alarm_settings();
  void update_4hour_rate_alarm_settings();
  //- method to parse alarms settings
  std::string get_alarm_settings(std::string cmd_to_send, double& value, short& alValid );

  //- HW data : periodic update
  void update_dose_data();
  void update_rate_data();
  void update_remainingDrops_data();
  void update_ABC1260_status();

  //- 4h RESET management
  void initialize_4h_reset_timer();	//- done at init
	void reset_i			 ();
	void unlock_cmds_i ();
  void decrypt_codes ();
  unsigned char decrypt(unsigned long int k);
  //- is commands availables
  bool m_reset_cmd_unlocked;
  //- password
  std::string m_pwdInClear;
  std::string m_pwdDecrypted;
  //- keys
  unsigned long int m_kn;
  unsigned long int m_kd;
  //- codes list to decrypt
  std::vector<unsigned long int> m_codesList;
	//- timer to lock reset cmd after kLOCK_CMDS_AFTER seconds
  yat::Timer m_unlockCMD_timer;
  //- timeout timer to send the reset cmd periodically each 4 hours
  yat::Timeout m_automatic_reset_timer;

  //- communication link proxy
  Tango::DeviceImpl*	 	m_host_device;
  std::string           m_proxyDeviceName;
  Communication *       m_com_p;
  std::string           m_protocole;

  //- structure used to share data between device and task
	ABC1260Data m_abcData;

	//- alarms settings
	Tango::DevVarDoubleStringArray* m_dose_alarm_settings;
	Tango::DevVarDoubleStringArray* m_rate_alarm_settings;
	Tango::DevVarDoubleStringArray* m_4h_rate_alarm_settings;

	std::string m_batteryCharge;

	//- APFEL model (or ABC1260)
	bool m_isABC1260;

  double m_gammaGain;

  std::string m_vialType;
  std::string m_vialName;
  long m_vialCalFact;
  long m_vialInitLoad;

  std::string m_serialNum;
  std::string m_firmwareVersion;

	//- State/status from the ABC (as string) to be parsed
  std::string m_responseStatusStr;

  //- disabled user alarms
  bool m_battery_disabled;
  bool m_vial_disabled;
  bool m_dose_disabled;
  bool m_rate_disabled;

	//- internal mutex
	yat::Mutex m_data_mutex;
	yat::Mutex m_serialNum_mutex;
	yat::Mutex m_firmwareVers_mutex;
	yat::Mutex m_vial_mutex;
  yat::Mutex m_status_mutex;
  yat::Mutex m_4hRateAlarmSettings_mutex;

};

} // namespace

#endif // _ABC1260_TASK_H_
