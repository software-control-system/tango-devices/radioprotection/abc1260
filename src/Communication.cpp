// ============================================================================
//
// = CONTEXT
//    TANGO Project - Communication Class
//
// = FILENAME
//    Communication.cpp
//
// = AUTHOR
//    X. Elattaoui
// ============================================================================

#include <yat/threading/Utilities.h> //- sleep
#include "Communication.hpp"

// ----------------------------------------------------------------------------
const std::size_t TIME_TO_WAIT_MSG_HANDLED     = 2000;        //- in ms
const long        TIME_TO_SLEEP_IN_SECONDS     = 0;           //- in second!
const long        TIME_TO_SLEEP_IN_NANOSECONDS = 300000000;   //- 300ms in nano seconds!
const long        SLEEP_AFTER_READ_IN_NANOSECONDS = 150000000;//- 150ms in nano seconds!
// ----------------------------------------------------------------------------
//- (Serial) commands
static const std::string SERIAL_WRITE_CMD     = "DevSerWriteString";
static const std::string SERIAL_READ_CMD      = "DevSerReadRaw";
static const std::string SERIAL_READ_ONLY_CMD = "DevSerReadString";
//- (Ethernet) command
static const std::string ETH_WRITE_CMD        = "Write";
static const std::string ETH_READ_CMD         = "Read";

namespace ABC1260_ns
{
// ============================================================================
// Communication::Communication
// ============================================================================
Communication::Communication (Tango::DeviceImpl * host_device,
                              std::string comDevName,
                              std::string protocole)
: yat4tango::TangoLogAdapter(host_device),
  m_com_protocole(NOT_DEFINED),
  m_protocole(protocole),
  m_dsproxy (0),
  m_host_dev(host_device),
  m_dev_name(comDevName),
  m_error(""),
  m_com_state(Tango::INIT)
{
  //- prepare write/read protocol specific commands
  if( m_protocole.find("SERIAL") != std::string::npos )
  {
    m_com_protocole = SERIAL;
    m_write_cmd     = SERIAL_WRITE_CMD;
    m_read_cmd      = SERIAL_READ_CMD;
    m_read_only_cmd = SERIAL_READ_ONLY_CMD;
  }
  else if( m_protocole.find("ETHERNET") != std::string::npos )
  {
    m_com_protocole = ETHERNET;
    m_write_cmd     = ETH_WRITE_CMD;
    m_read_cmd      = ETH_READ_CMD;
    m_read_only_cmd = m_read_cmd;
  }
}

// ============================================================================
// Communication::~Communication
// ============================================================================
Communication::~Communication (void)
{
  delete_proxy();
}

// ============================================================================
// Communication:create_proxy
// ============================================================================
void Communication::create_proxy()
{
  if ( !m_dev_name.empty() )
  {
    try
    {
      //- create proxy
      m_dsproxy = new Tango::DeviceProxyHelper(m_dev_name, m_host_dev);
      m_com_state = Tango::ON;
    }
    catch(Tango::DevFailed& df)
    {
      m_com_state = Tango::FAULT;
      m_error = "Failed to create proxy on \"" + m_dev_name + "\" : \n";
      m_error+= std::string(df.errors[0].desc);
      ERROR_STREAM << m_error << std::endl;
      delete_proxy();
      return;
    }
  }
}

// ============================================================================
// Communication::delete_proxy
// ============================================================================
void Communication::delete_proxy()
{
  if ( m_dsproxy )
  {
    delete m_dsproxy;
    m_dsproxy = 0;
  }
}

// ============================================================================
// Communication::check_proxy
// ============================================================================
void Communication::check_proxy()
{
  if ( m_dsproxy )
  {
    try
    {
      //- check device is up !
      m_dsproxy->get_device_proxy()->ping();
    }
    catch(Tango::DevFailed& df)
    {
      m_com_state = Tango::ALARM;
      //- store the Tango error
      m_error = std::string(df.errors[0].desc);
      ERROR_STREAM << m_error << std::endl;
      throw;
    }
    
    try
    {
      //- Serial device : read status to get updated state!
      if( m_com_protocole == SERIAL)
      {
        m_dsproxy->get_device_proxy()->status();
      }
      //- check device is up !
      m_com_state = m_dsproxy->get_device_proxy()->state();
      if( m_com_state != Tango::OPEN )
      {
        m_com_state = Tango::ALARM;
      	m_error = "Cannot read back state of device \"" + m_dev_name + "\" or its Tango state is not OPEN.\n";
      }
    }
    catch(Tango::DevFailed& df)
    {
      m_com_state = Tango::ALARM;
      m_error = "Cannot read back state of device \"" + m_dev_name + "\" \n";
      m_error+= std::string(df.errors[0].desc);
      ERROR_STREAM << m_error << std::endl;
      throw;
    }
  }
}

// ============================================================================
// Communication::write_read
// ============================================================================
std::string Communication::write_read(const std::string& cmd)
{
  static std::string empty("");
  std::string response("");

  if ( !m_dsproxy )
  {
    {
      m_com_state = Tango::FAULT;
      m_error = "Communication link proxy is not created.";
    }
    Tango::Except::throw_exception("COMMUNICATION_ERROR",
                                   m_error,
                                   "Communication::write_read");
  }

  try
  {
    check_proxy();
  }
  catch(...)
  {
    return empty;
  }

  try
  {
    if ( m_dsproxy )
    {
      DEBUG_STREAM << "Communication::WRITE_READ -> for cmd *" << cmd << "*" << std::endl;
      
      response.clear();
      //- send the cmd 
      m_dsproxy->command_in(m_write_cmd, cmd);
      //- sleep to let RS adapter to switch mode
      yat::ThreadingUtilities::sleep(TIME_TO_SLEEP_IN_SECONDS, TIME_TO_SLEEP_IN_NANOSECONDS);
      //- and read back its response
      m_dsproxy->command_out(m_read_cmd, response);
      //- sleep to let RS adapter to switch mode
      yat::ThreadingUtilities::sleep(TIME_TO_SLEEP_IN_SECONDS, SLEEP_AFTER_READ_IN_NANOSECONDS);
      
      DEBUG_STREAM << "Communication::WRITE_READ -> resp *" << response << "*" << std::endl;
    }
  }
  catch(Tango::DevFailed & df)
  {
    m_com_state = Tango::ALARM;
    m_error = "Communication::write_read received a DevFailed exception : cmd $" + cmd + "$ with resp $" + response + "\n";
    m_error+= std::string(df.errors[0].desc);
    ERROR_STREAM << m_error << std::endl;
  }
  catch(...)
  {
    m_com_state = Tango::ALARM;
    m_error = "Communication::write_read received a [...] exception.\n";
    ERROR_STREAM << m_error << std::endl;
  }
 
  return response;
}

// ============================================================================
// Communication::read
// ============================================================================
std::string Communication::read()
{
  static std::string empty("");
  std::string response("");

  if ( !m_dsproxy )
  {
    {
      m_com_state = Tango::FAULT;
      m_error = "Communication link proxy is not created.";
    }
    Tango::Except::throw_exception("COMMUNICATION_ERROR",
                                   m_error,
                                   "Communication::write_read");
  }

  try
  {
    check_proxy();
  }
  catch(...)
  {
    return empty;
  }

  try
  {
    if ( m_dsproxy )
    {       
      m_dsproxy->command_out(m_read_only_cmd, response);
      DEBUG_STREAM << "Communication::READ O -> resp *" << response << "*" << std::endl;
    }
  }
  catch(Tango::DevFailed & df)
  {
    m_com_state = Tango::ALARM;
    m_error = "Communication::read received a DevFailed exception : resp [" + response + "]\n";
    m_error+= std::string(df.errors[0].desc);
    ERROR_STREAM << m_error << std::endl;
  }
  catch(...)
  {
    m_com_state = Tango::ALARM;
    m_error = "Communication::write_read received a [...] exception.\n";
    ERROR_STREAM << m_error << std::endl;
  }
  
  return response;
}

// ============================================================================
// Communication::clear_error
// ============================================================================
void Communication::clear_error()
{
  m_com_state = Tango::ON;
  m_error.clear();
}

} //- end namespace
